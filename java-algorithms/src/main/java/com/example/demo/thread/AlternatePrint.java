package com.example.demo.thread;

import lombok.SneakyThrows;

/**
 * @Description: 两线程交替打印
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/25 10:21
 * @UpdateDate: 2022/3/25 10:21
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class AlternatePrint {

    Object lock = new Object();

    public void start() {
        MyThreadOutLetter myThreadOutLetter = new MyThreadOutLetter();
        MyThreadOutNumber myThreadOutNumber = new MyThreadOutNumber();
        myThreadOutLetter.start();
        myThreadOutNumber.start();
    }

    class MyThreadOutLetter extends Thread {

        @SneakyThrows
        @Override
        public void run() {
            for (char c = 'A'; c < 'Z'; c++) {
                synchronized (lock) {
                    System.out.println(c);
                    lock.notify();
                    lock.wait();
                }
            }
        }
    }

    class MyThreadOutNumber extends Thread {
        @SneakyThrows
        @Override
        public void run() {
            for (int i = 0; i < 26; i++) {
                synchronized (lock) {
                    System.out.println(i);
                    lock.notify();
                    lock.wait();
                }
            }
        }
    }

    public static void main(String[] args) {
        AlternatePrint alternatePrint = new AlternatePrint();
        alternatePrint.start();

    }
}
