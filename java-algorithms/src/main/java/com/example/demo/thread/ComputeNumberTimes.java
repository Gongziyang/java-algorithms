package com.example.demo.thread;

import lombok.SneakyThrows;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/25 12:19
 * @UpdateDate: 2022/3/25 12:19
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 利用ForkJoin计算某个数的出现次数
 */
public class ComputeNumberTimes {

    public void compute(){

    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Integer[] array = new Integer[]{11,55,551,55511};
        ZTask zTask = new ZTask(array,0,array.length - 1);
        ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
        System.out.println(forkJoinPool.invoke(zTask));
    }

    static class ZTask extends RecursiveTask<Integer> {

        private Integer[] array;
        private int start;
        private int end;

        public ZTask(Integer[] array, int start, int end) {
            super();
            this.array = array;
            this.start = start;
            this.end = end;
        }

        @SneakyThrows
        @Override
        protected Integer compute() {
            int result = 0;
            if (end - start >= 1) {
                int mid = (start + end) / 2;
                ZTask t1 = new ZTask(array, start, mid);
                ZTask t2 = new ZTask(array, mid + 1, end);
                invokeAll(t1, t2);
                result = t1.get() + t2.get();
            } else {
                result = digitCheck(array[end]);
            }
            return result;
        }

        private Integer digitCheck(Integer number) {
            int count = 0;
            while (number != 0) {
                if (number % 10 == 1) {
                    count++;
                }
                number = number / 10;
            }
            return count;
        }
    }
}
