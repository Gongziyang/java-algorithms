package com.example.demo.tree.node;

import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/26 11:23
 * @UpdateDate: 2022/3/26 11:23
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 跳表节点
 */
@Data
public class SkipListNode<T> {

    private T data;

    /**
     * 后继节点
     */
    private SkipListNode<T> next;

    /**
     * 下层节点
     */
    private SkipListNode<T> down;

    public SkipListNode(T data) {
        this.data = data;
    }

    /**
     * 当前对象的data大于 传入data时返回1 等于返回0 小于返回-1
     *
     * @param data
     * @return
     */
    public int compare(T data) {
        //遇到最右端节点时 一定是大于任何值
        if (null == this.data) {
            return 1;
        }
        if (data instanceof String) {
            if (((String) this.data).length() > ((String) data).length()) {
                return 1;
            } else if (((String) this.data).length() < ((String) data).length()) {
                return -1;
            } else {
                return 0;
            }
        } else if (data instanceof Integer) {
            if ((Integer) this.data > (Integer) data) {
                return 1;
            } else if ((Integer) this.data < (Integer) data) {
                return -1;
            } else {
                return 0;
            }
        } else {
            throw new RuntimeException("UnSupported Param Type");
        }
    }
}
