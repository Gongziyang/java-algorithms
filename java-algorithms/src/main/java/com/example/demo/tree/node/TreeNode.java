package com.example.demo.tree.node;

import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 15:14
 * @UpdateDate: 2022/3/24 15:14
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class TreeNode<T> {

    /**
     * 节点数据
     */
    T data;

    /**
     * 左子节点
     */
    TreeNode leftChildNode;

    /**
     * 右子节点
     */
    TreeNode rightChildNode;


    /**
     * 线索化二叉树
     */
    /**
     * 左子节点类型 0子树 1前驱节点
     */
    int leftType;

    /**
     * 右子节点类型 0子树 1后继节点
     */
    int rightType;
}
