package com.example.demo.tree;

import com.example.demo.tree.node.SkipListNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/26 12:49
 * @UpdateDate: 2022/3/26 12:49
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 跳表实现
 */
public class SkipList<T> {

    private ReentrantLock lock = new ReentrantLock(true);

    /**
     * 随机数生成器
     */
    private Random random = new Random();

    /**
     * 当前跳表层数
     */
    private int level;

    /**
     * 根节点
     */
    private SkipListNode<T> root;

    /**
     * 初始化跳表
     */
    public SkipList() {
        SkipListNode<T> tail = new SkipListNode<>(null);
        root.setNext(tail);
    }

    /**
     * 查找
     *
     * @param data
     * @return
     */
    public SkipListNode<T> search(T data) {
        SkipListNode<T> index = root;
        while (null != index) {
            if (index.getNext().compare(data) == 0) {
                //命中直接返回
                return index.getNext();
            } else if (index.getNext().compare(data) < 0) {
                index = index.getNext();
            } else {
                index = index.getDown();
            }
        }
        return null;
    }

    /**
     * @param data
     */
    public void insert(T data) {
        lock.lock();
        try {
            //标记节点，便于插入
            List<SkipListNode<T>> marks = new ArrayList<>(16);
            SkipListNode<T> index = root;
            while (null != index) {
                //如果存在，直接return
                if (index.getNext().compare(data) == 0) {
                    return;
                } else if (index.getNext().compare(data) > 0) {
                    //标记
                    marks.add(index);
                    index = index.getDown();
                } else {
                    index = index.getNext();
                }
            }
            //添加新节点
            add(data, marks);
        } finally {
            lock.unlock();
        }
    }

    private void add(T data, List<SkipListNode<T>> marks) {
        int random = random();
        //记录random与；level的差值
        int overflow = 0;
        //当前节点层数是否大于当前跳表层数
        if (random > level) {
            overflow = random - level;
            level = random;
        }
        //记录新添加节点那一列的最顶部节点
        SkipListNode<T> top = null;
        //用来调整纵向关系
        SkipListNode<T> tmp = null;
        //在原有的跳表上添加新节点
        for (SkipListNode<T> mark : marks) {
            //设置nNode的横向关系
            SkipListNode<T> nNode = new SkipListNode<>(data);
            SkipListNode<T> swap = null;
            swap = mark.getNext();
            mark.setNext(nNode);
            nNode.setNext(swap);
            //设置nNode的下级关系
            if (null == tmp) {
                tmp = nNode;
            } else {
                tmp.setDown(nNode);
                tmp = nNode;
            }
            //记录顶部节点
            if (null == top) {
                top = nNode;
            }
        }
        //random比level大时更新root节点
        tmp = null;
        //添加多余的那部分节点层数
        for (int i = 0; i < overflow; i++) {
            SkipListNode<T> endPointNode = new SkipListNode<>(null);
            SkipListNode<T> nNode = new SkipListNode<>(data);
            endPointNode.setDown(root);
            root = endPointNode;
            root.setNext(nNode);
            nNode.setNext(new SkipListNode<>(null));

            if (null == tmp) {
                tmp = nNode;
            } else {
                tmp.setDown(nNode);
                tmp = nNode;
            }
        }
        tmp.setDown(top);
        //complete...
    }

    /**
     * 删除节点 成功返回true，若未找到返回false
     *
     * @param data 待删除元素
     * @return 删除成功 true 删除失败 false
     */
    public boolean delete(T data) {
        lock.lock();
        try {
            SkipListNode<T> index = root;
            SkipListNode<T> mark = null;
            //找出元素
            while (null != index) {
                if (index.getNext().compare(data) == 0) {
                    mark = index;
                } else if (index.getNext().compare(data) > 0) {
                    index = index.getNext();
                } else {
                    index = index.getDown();
                }
            }
            if (null == mark) {
                return false;
            } else {
                while (null != mark) {
                    mark.setNext(mark.getNext().getNext());
                    mark = mark.getDown();
                }
                return true;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * 随机数生成 （类似抛硬币）  平均层数为 1/p = 2
     *
     * @return 节点层数
     */
    private int random() {
        //默认层数为1
        int level = 1;
        while (random.nextInt(2) == 1) {
            level++;
        }
        return level;
    }
}
