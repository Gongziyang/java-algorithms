package com.example.demo.tree;

import com.example.demo.tree.node.TreeNode;
import lombok.Data;

import java.util.List;

/**
 * @Description: 实现了二叉树的前中后序遍历、中序线索化二叉树及查找、排序二叉树查找、添加、AVL树的构建
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 15:18
 * @UpdateDate: 2022/3/24 15:18
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class TreeRangeUtil {

    /**
     * 线索化二叉树时，保存每个线程的钱去节点
     */
    static ThreadLocal<TreeNode> preNode = new ThreadLocal<>();

    /**
     * 前驱结点的key
     */
    static final String PRE_NODE = "node_prefix";

    /**
     * 前序遍历
     */
    public static void frontRange(TreeNode node) {
        if (null != node) {
            System.out.println(node.getData());
            if (null != node.getLeftChildNode()) {
                frontRange(node.getLeftChildNode());
            }
            if (null != node.getRightChildNode()) {
                frontRange(node.getRightChildNode());
            }
        }
    }

    /**
     * 中序遍历
     */
    public static void midRange(TreeNode node) {
        if (null != node) {
            if (null != node.getLeftChildNode()) {
                midRange(node.getLeftChildNode());
            }
            System.out.println(node.getData());
            if (null != node.getRightChildNode()) {
                midRange(node.getRightChildNode());
            }
        }
    }

    /**
     * 后序遍历
     */
    public static void behindRange(TreeNode node) {
        if (null != node) {
            if (null != node.getLeftChildNode()) {
                behindRange(node.getLeftChildNode());
            }
            if (null != node.getRightChildNode()) {
                behindRange(node.getRightChildNode());
            }
            System.out.println(node.getData());
        }
    }

    /**
     * 中序线索化二叉树
     */
    public static void threadsTreeMid(TreeNode node) {
        if (null == node) {
            return;
        }
        //左子树
        threadsTreeMid(node.getLeftChildNode());

        if (null == node.getLeftChildNode()) {
            node.setLeftChildNode(preNode.get());
            node.setLeftType(1);
        }
        if (preNode.get() != null && preNode.get().getRightChildNode() == null) {
            preNode.get().setRightChildNode(node);
            preNode.get().setRightType(1);
        }

        //不管是什么顺序，当前节点逻辑执行完之后切换到下一个节点，故当前节点为下一个节点的前驱节点。
        preNode.set(node);
        //右子树
        threadsTreeMid(node.getRightChildNode());
    }

    /**
     * rangeByThreadMid 中序遍历线索化二叉树
     */
    static void rangeByThreadMid(TreeNode node) {
        if (null == node) {
            return;
        }
        while (null != node) {
            //中序线索化时从最左边的叶子节点开始遍历
            while (node.getLeftType() == 0) {
                node = node.getLeftChildNode();
            }
            System.out.println(node.getData());
            //遍历右边时 不能从rightType = 1的节点开始下一次循环，可能产生死循环
            while (node.getRightType() == 1) {
                node = node.getRightChildNode();
                System.out.println(node.getData());
            }
            node = node.getRightChildNode();
        }
    }

    /**
     * 二叉排序树（二叉搜索树）
     */
    private class SortedTree<T> {
        private T data;
        private SortedTree<T> leftChild;
        private SortedTree<T> rightChild;

        /**
         * 批量新增
         *
         * @param sortedTrees
         */
        public void addBatch(List<SortedTree<T>> sortedTrees) {
            for (SortedTree<T> sortedTree : sortedTrees) {
                this.add(sortedTree);
            }
        }

        private void add(SortedTree<T> sortedTree) {
            if (this.data instanceof String) {
                if (((String) this.data).length() > ((String) sortedTree.data).length()) {
                    if (null == this.leftChild) {
                        this.leftChild = sortedTree;
                        return;
                    }
                    this.leftChild.add(sortedTree);
                } else if (((String) this.data).length() < ((String) sortedTree.data).length()) {
                    if (null == this.rightChild) {
                        this.rightChild = sortedTree;
                        return;
                    }
                    rightChild.add(sortedTree);
                } else {
                    this.data = sortedTree.data;
                    return;
                }
            } else if (this.data instanceof Integer) {
                if (((Integer) this.data) > ((Integer) sortedTree.data)) {
                    if (null == this.leftChild) {
                        this.leftChild = sortedTree;
                        return;
                    }
                    this.leftChild.add(sortedTree);
                } else if (((Integer) this.data) < ((Integer) sortedTree.data)) {
                    if (null == this.rightChild) {
                        this.rightChild = sortedTree;
                        return;
                    }
                    this.rightChild.add(sortedTree);
                } else {
                    this.data = sortedTree.data;
                    return;
                }
            } else {
                throw new RuntimeException("UnSupported data type. just for(String or Integer)");
            }
        }

        /**
         * 查找
         */
        public SortedTree<T> search(SortedTree<T> sortedTree) {
            if (null == sortedTree) {
                return null;
            }
            if (this.data instanceof String) {
                if (((String) this.data).length() > ((String) sortedTree.data).length()) {
                    if (null == this.leftChild) {
                        return null;
                    }
                    this.leftChild.search(sortedTree);
                } else if (((String) this.data).length() < ((String) sortedTree.data).length()) {
                    if (null == this.rightChild) {
                        return null;
                    }
                    this.rightChild.search(sortedTree);
                } else {
                    return this;
                }
            } else if (this.data instanceof Integer) {
                if (((Integer) this.data) > ((Integer) sortedTree.data)) {
                    if (null == this.leftChild) {
                        return null;
                    }
                    this.leftChild.search(sortedTree);
                } else if (((Integer) this.data) < ((Integer) sortedTree.data)) {
                    if (null == this.rightChild) {
                        return null;
                    }
                    this.rightChild.search(sortedTree);
                } else {
                    return this;
                }
            } else {
                throw new RuntimeException("UnSupported data type. just for(String or Integer)");
            }
            return null;
        }
    }

    /**
     * 默认支持 String 和 Integer类型 String比较规则为 字符串长度
     * 实现平衡有两种方式 单旋转 和双旋转 该方法使用双旋转实现
     */
    static class AVLTree<T> {
        private T data;
        private AVLTree<T> leftChild;
        private AVLTree<T> rightChild;

        /**
         * 获取树高度
         */
        private int getTreeHigh() {
            return Math.max(this.leftChild == null ? 0 : this.leftChild.getTreeHigh(),
                    this.rightChild == null ? 0 : this.rightChild.getTreeHigh()) + 1;
        }

        /**
         * 获取左子树高度
         */
        private int getLeftChildHigh() {
            if (null == this.leftChild) {
                return 0;
            }
            return this.leftChild.getTreeHigh();
        }

        /**
         * 获取右子树高度
         */
        private int getRightChildHigh() {
            if (null == this.rightChild) {
                return 0;
            }
            return this.rightChild.getTreeHigh();
        }

        /**
         * 批量新增
         */
        public void addBatch(List<AVLTree<T>> avlTrees) {
            for (AVLTree<T> avlTree : avlTrees) {
                add(avlTree);
            }
        }

        private void add(AVLTree<T> avlTree) {
            if (data instanceof String) {
                if (((String) data).length() > ((String) avlTree.data).length()) {
                    if (null == leftChild) {
                        leftChild = avlTree;
                        return;
                    }
                    leftChild.add(avlTree);
                } else if (((String) data).length() < ((String) avlTree.data).length()) {
                    if (null == rightChild) {
                        rightChild = avlTree;
                        return;
                    }
                    rightChild.add(avlTree);
                } else {
                    data = avlTree.data;
                }
                //检查是否平衡
                if (getLeftChildHigh() - getRightChildHigh() >= 2) {
                    if (leftChild != null && leftChild.getLeftChildHigh() < leftChild.getRightChildHigh()) {
                        //先进行左子节点的左旋转，在进行当前节点右旋转
                        leftChild.leftRotate();
                        rightRotate();
                    } else {
                        //否则当前节点直接进行右旋
                        leftRotate();
                    }
                }

                if (getRightChildHigh() - getLeftChildHigh() >= 2) {
                    if (rightChild != null && rightChild.getLeftChildHigh() > rightChild.getRightChildHigh()) {
                        //右子节点先进行右旋 ，当前节点再进行左旋
                        rightChild.rightRotate();
                        leftRotate();
                    } else {
                        //否则当前节点直接进左旋
                        leftRotate();
                    }
                }
            } else if (data instanceof Integer) {
                if (((Integer) data) > ((Integer) avlTree.data)) {
                    if (null == leftChild) {
                        leftChild = avlTree;
                        return;
                    }
                    this.leftChild.add(avlTree);
                } else if (((Integer) data) < ((Integer) avlTree.data)) {
                    if (null == rightChild) {
                        rightChild = avlTree;
                        return;
                    }
                    rightChild.add(avlTree);
                } else {
                    data = avlTree.data;
                }
            } else {
                throw new RuntimeException("UnSupported data type. just for(String or Integer)");
            }
        }

        /**
         * 左旋转 （需要保持当前节点 位置不变 从而保证头节点位置不变）
         */
        public void leftRotate() {
            AVLTree<T> leftNode = new AVLTree<>();
            leftNode.data = data;
            leftNode.leftChild = leftChild;
            leftNode.rightChild = rightChild.leftChild;

            data = rightChild.data;
            rightChild = rightChild.rightChild;
            leftChild = leftNode;
        }

        /**
         * 右旋转
         */
        public void rightRotate() {
            //先保存当前节点的右子节点的左子节点
            AVLTree<T> rightNode = new AVLTree<>();
            rightNode.data = data;
            rightNode.rightChild = rightChild;
            rightNode.leftChild = leftChild.rightChild;

            data = leftChild.data;
            leftChild = leftChild.leftChild;
            rightChild = rightNode;
        }
    }
}
