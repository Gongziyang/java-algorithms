package com.example.demo.about_string;

/**
 * @Description: 实现一些关于字符串相关的算法类
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/28 11:50
 * @UpdateDate: 2022/3/28 11:50
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class StringAlgorithmUtils {

    /**
     * 找出 出现最多的 字符
     *
     * @return Object[0]出现次数 Object[1] 目标字符
     */
    public static Object[] mostFrequencyChar(String source) {
        String cStr = "";
        int number = -1;
        String tmp = null;
        for (char c : source.toCharArray()) {
            tmp = source.replaceAll(c + "", "");
            int cLen = source.length() - tmp.length();
            if (cLen > number) {
                number = cLen;
                cStr = String.valueOf(c);
            }
            source = tmp;
        }
        return new Object[]{number, cStr};
    }
}
