package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 11:26
 * @UpdateDate: 2022/3/24 11:26
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class SimpleSelectSortImpl implements Sorter {

    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        sorted(integers);
        return integers;
    }

    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            sorted(targetSource);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }

    public void sorted(Integer[] source) {
        for (int i = 0; i < source.length; i++) {
            int miniIndex = i;
            for (int j = i + 1; j < source.length; j++) {
                if (source[j] < source[miniIndex]) {
                    miniIndex = j;
                }
            }
            if (miniIndex != i) {
                //swap...
                int temp = source[i];
                source[i] = source[miniIndex];
                source[miniIndex] = temp;
            }
        }
    }
}
