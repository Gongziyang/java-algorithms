package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 11:02
 * @UpdateDate: 2022/3/24 11:02
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class QuickSortimpl implements Sorter {

    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            quickSort(targetSource, 0, targetSource.length);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        quickSort(integers, 0, integers.length);
        return integers;
    }

    void quickSort(Integer[] target, int from, int end) {
        if (from != end) {
            int start = from;
            int finish = end;
            Integer standard = target[from];
            while (start < finish) {
                while (start < finish && target[finish] > standard) {
                    finish--;
                }
                target[start] = target[finish];
                while (start < finish && target[start] < standard) {
                    start++;
                }
                target[finish] = target[start];
            }
            target[start] = standard;
            //one quicksort end...
            //left quicksort...
            quickSort(target, from, start - 1);
            //right quicksort...
            quickSort(target, start + 1, end);
        }
    }
}
