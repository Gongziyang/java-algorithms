package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 13:39
 * @UpdateDate: 2022/3/24 13:39
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class HeapSortImpl implements Sorter {

    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        heapSort(integers);
        return integers;
    }

    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            heapSort(targetSource);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }

    /**
     * 构造大顶堆
     *
     * @param source
     */
    public void createHeap(Integer[] source) {
        int leafIndex = (source.length - 2) / 2;
        for (int i = leafIndex; leafIndex >= 0; leafIndex--) {
            compareAndSwap(leafIndex, source, source.length);
        }
    }

    /**
     * 比较并交换，找出最大的结点。
     * 完全二叉树 ：
     *  所有叶子节点都在最后一层
     *  最后一层的叶子节点全都分布在最左边且连续
     *
     * @param index
     */
    public void compareAndSwap(int index, Integer[] source, int range) {
        int leftChildNode = 2 * index + 1;
        int rightChildNode = 2 * index + 2;
        int descIndex = index;
        if (leftChildNode < range && source[leftChildNode] < source[descIndex]) {
            descIndex = leftChildNode;
        }
        if (rightChildNode < range && source[rightChildNode] < source[descIndex]) {
            descIndex = rightChildNode;
        }
        //swap...
        Integer temp = source[index];
        source[index] = source[descIndex];
        source[descIndex] = temp;
        //重复比较
        compareAndSwap(descIndex, source, range);
    }

    public void heapSort(Integer[] source) {
        createHeap(source);
        for (int i = 0; i < source.length; i++) {
            source[i] = source[0];
            //swap...
            Integer temp = source[source.length - i - 1];
            source[source.length - i - 1] = source[0];
            source[0] = temp;
            //调整堆
            compareAndSwap(0, source, source.length - i - 1);
        }
    }
}
