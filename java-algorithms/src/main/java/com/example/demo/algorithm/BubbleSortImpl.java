package com.example.demo.algorithm;
import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 10:38
 * @UpdateDate: 2022/3/24 10:38
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class BubbleSortImpl implements Sorter {

    private ReentrantLock lock = new ReentrantLock(true);

    /**
     * 同步 对同一共享资源进行排序
     *
     * @param targetSource
     */
    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            sort(targetSource);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }

    }

    /**
     * 异步 返回新的数组对象
     *
     * @param targetSource
     * @return
     */
    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        sorted(integers);
        return integers;
    }

    private void sorted(Integer[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
