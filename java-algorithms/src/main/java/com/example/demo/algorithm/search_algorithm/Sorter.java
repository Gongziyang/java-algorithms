package com.example.demo.algorithm.search_algorithm;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 11:25
 * @UpdateDate: 2022/3/24 11:25
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public interface Sorter {

    Integer[] sort(Integer[] targetSource);

    void sortInternal(Integer[] targetSource);
}
