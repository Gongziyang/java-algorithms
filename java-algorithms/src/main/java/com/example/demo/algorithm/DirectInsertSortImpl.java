package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 11:40
 * @UpdateDate: 2022/3/24 11:40
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class DirectInsertSortImpl implements Sorter {

    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        sorted(integers);
        return integers;

    }

    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            sorted(targetSource);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }

    private void sorted(Integer[] source) {
        for (int i = 1; i < source.length; i++) {
            if (source[i] < source[i - 1]) {
                for (int j = i; j > 0 && source[j] < source[j - 1]; j--) {
                    //swap
                    int temp = source[j];
                    source[j] = source[j - 1];
                    source[j - 1] = temp;
                }
            }
        }
    }
}
