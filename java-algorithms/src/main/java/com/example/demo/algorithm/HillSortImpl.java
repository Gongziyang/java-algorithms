package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 12:06
 * @UpdateDate: 2022/3/24 12:06
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class HillSortImpl implements Sorter {


    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        sorted(integers);
        return integers;
    }

    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            sorted(targetSource);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }

    public void sorted(Integer[] source) {
        int stepSize = source.length / 2;
        while (0 != stepSize) {
            for (int i = stepSize; i < source.length; i += stepSize) {
                if (source[i] < source[i - stepSize]) {
                    for (int j = i; i > 0 && source[j] < source[j - stepSize]; j -= stepSize) {
                        //swap...
                        int temp = source[j];
                        source[j] = source[j - stepSize];
                        source[j - stepSize] = temp;
                    }
                }
            }
        }
    }
}
