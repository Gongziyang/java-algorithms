package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.BinarySearch;

/**
 * @Description: 二分查找
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 10:14
 * @UpdateDate: 2022/3/24 10:14
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class BinarySearchImpl implements BinarySearch {
    /**
     * 待排序数组
     */
    private Integer[] source;

    public BinarySearchImpl(Integer[] targetSource) {
        this.source = targetSource;
    }

    public void setSource(Integer[] targetSource) {
        this.source = targetSource;
    }

    @Override
    public Integer search(Integer targetNum) {
        if (null != source) {
            return search(source, targetNum);
        }
        throw new RuntimeException("targetNumber can not be null,construct it!");
    }

    @Override
    public Integer search(Integer[] sortedArray, Integer targetNumber) {
        if (null == targetNumber) {
            throw new RuntimeException("targetNumber can not be null,check your parameter");
        }
        int from = 0;
        int end = sortedArray.length - 1;
        int mid = -1;
        while (from <= end) {
            mid = (from + end) / 2;
            if (targetNumber.equals(mid)) {
                return mid;
            } else if (targetNumber > mid) {
                from = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return mid;
    }
}
