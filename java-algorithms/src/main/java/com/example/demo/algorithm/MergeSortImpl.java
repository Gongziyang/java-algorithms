package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 12:18
 * @UpdateDate: 2022/3/24 12:18
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class MergeSortImpl implements Sorter {

    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        startSort(integers, 0, integers.length);
        return integers;
    }

    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            startSort(targetSource, 0, targetSource.length);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }

    public void startSort(Integer[] targetSource, int from, int end) {
        if (from < end) {
            int mid = (from + end) / 2;
            startSort(targetSource, from, mid);
            startSort(targetSource, mid + 1, end);
            merge(targetSource, from, mid, end);
        }
    }

    public void merge(Integer[] source, int from, int mid, int end) {
        Integer[] leftSource = Arrays.copyOfRange(source, from, mid);
        Integer[] rightSource = Arrays.copyOfRange(source, mid + 1, end);
        Integer[] desc = new Integer[end - from + 1];
        int i = 0, j = 0, count = 0;
        while (i < leftSource.length || j < rightSource.length) {
            boolean iNum = i < leftSource.length;
            boolean jNum = j < rightSource.length;
            if (iNum == jNum) {
                if (leftSource[i] < rightSource[j]) {
                    desc[count++] = rightSource[j++];
                } else {
                    desc[count++] = leftSource[i++];
                }
            } else {
                if (iNum) {
                    desc[count++] = leftSource[i++];
                } else {
                    desc[count++] = rightSource[j++];
                }
            }
        }
    }
}
