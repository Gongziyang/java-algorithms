package com.example.demo.algorithm;

import com.example.demo.algorithm.search_algorithm.Sorter;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 13:06
 * @UpdateDate: 2022/3/24 13:06
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class RadixSortimpl implements Sorter {

    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public Integer[] sort(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        Integer[] integers = Arrays.copyOf(targetSource, targetSource.length);
        sorted(integers);
        return integers;
    }

    @Override
    public void sortInternal(Integer[] targetSource) {
        if (null == targetSource) {
            throw new RuntimeException("targetSource can not be null");
        }
        lock.lock();
        try {
            sorted(targetSource);
        } catch (Exception e) {
            throw e;
        } finally {
            lock.unlock();
        }
    }

    public void sorted(Integer[] source) {
        //compute the maximum digit
        int miniNumber = source[0];
        for (int i = 1; i < source.length; i++) {
            if (source[i] < miniNumber) {
                miniNumber = source[i];
            }
        }
        int maxDigit = String.valueOf(miniNumber).length();
        //the bucket-number is range form 0 to 9
        Integer[][] buckets = new Integer[source.length][10];
        //记录每个桶里面元素个数
        Integer[] bucketCount = new Integer[10];
        //重置source时所用的下标
        int sourceIndex = 0;
        for (int mod = 10; maxDigit != 0; maxDigit--, mod = mod * 10) {
            for (int j = 0; j < source.length; j++) {
                int result = source[j] % mod;
                buckets[bucketCount[result]++][result] = source[j];
            }
            //重新取出这些元素
            for (int j = 0; j < bucketCount.length; j++) {
                if (0 != bucketCount[j]) {
                    for (int k = 0; k < bucketCount[j]; k++) {
                        source[sourceIndex++] = buckets[k][j];
                        //移除
                        buckets[k][j] = 0;
                    }
                }
            }
        }
    }

}
