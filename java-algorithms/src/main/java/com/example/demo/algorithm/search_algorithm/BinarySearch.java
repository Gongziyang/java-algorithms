package com.example.demo.algorithm.search_algorithm;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/24 10:20
 * @UpdateDate: 2022/3/24 10:20
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public interface BinarySearch {

    Integer search(Integer target);

    Integer search(Integer[] sortedArray,Integer targetNumber);
}
